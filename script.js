function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level
	this.attack = level;
	//methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		let subtractedHealth = target.health - this.attack;
		target.health = subtractedHealth;
		if(target.health > 5) {
			console.log(target.name + "'s health is now reduced to " + subtractedHealth);
		} else {
			target.faint()
		}
		
	},
	this.faint = function() {
		console.log(this.name + " fainted.");
	} 
} 

//Creates a new instance of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 8);
let bulbasaur = new Pokemon("Bulbasaur", 4);
let charmander = new Pokemon("Charmander", 2);

// First Round
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
charmander.tackle(squirtle);
